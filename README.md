# simple-pendulum

Simple implementation of an animation of a pendulum in Javascript

[See live here](https://hworden-showcase.gitlab.io/simple-pendulum/app.html)
