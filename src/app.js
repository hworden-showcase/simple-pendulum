function resize() {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
}

function main() {
  const canvas = document.getElementById("canvas");
  resize();
  const stencil = new Stencil(canvas.getContext("2d"));
  const origin = {
    x: canvas.width / 2,
    y: 0
  }
  let aVel = 0.0;
  const pendulums = [
    new Pendulum(origin, Math.PI / 4, 100),
    new Pendulum(origin, Math.PI / 8, 200),
    new Pendulum(origin, Math.PI / 16, 300),
    new Pendulum(origin, Math.PI / 32, 400)
  ];
  const step = function(timestamp) {
    stencil.clear();
    for (pendulum of pendulums) {
      let xPos = pendulum.xPos();
      let yPos = pendulum.yPos();
      stencil.drawLine(origin.x, origin.y, xPos, yPos);
      stencil.drawCircle(xPos, yPos, 15);
      pendulum.tick();
    }
    window.requestAnimationFrame(step);
  };
  window.requestAnimationFrame(step);
}


class Pendulum {
  constructor(origin, theta, length) {
    this.length = length;
    this.origin = origin;
    this.gravity = -0.009;
    this.aVel = 0.0;
    this.theta = theta;
  }
  xPos(theta) {
    return (this.length * Math.sin(this.theta)) + this.origin.x;
  }
  yPos(theta) {
    return (this.length * Math.cos(this.theta)) + this.origin.y;
  }
  angAcc() {
    return this.gravity * Math.sin(this.theta);
  }
  tick() {
    this.aVel += this.angAcc();
    this.theta += this.aVel;
  }
}

class Stencil {
  constructor(ctx) {
    this.ctx = ctx;
  }

  drawCircle(xPos, yPos, radius) {
    this.ctx.beginPath();
    this.ctx.arc(xPos, yPos, radius, 0, 2 * Math.PI);
    this.ctx.stroke();
  }

  drawLine(fromX, fromY, toX, toY) {
    this.ctx.beginPath();
    this.ctx.moveTo(fromX, fromY);
    this.ctx.lineTo(toX, toY);
    this.ctx.stroke();
  }

  clear() {
    this.ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
}

window.onload = main;
